import React from 'react';
import ReactDOM from 'react-dom/client';
import{
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";
import './index.css';

import reportWebVitals from './reportWebVitals';
import App from './App';
import List from './Blog/List';
import Detail from './Blog/Detail';
import Index from './Member/Index';
import Update from './Member/Update';
import AddProduct from './Product/AddProduct';
import MyProduct from './Product/MyProduct';
import HandleErr from './Member/HandleErr';
import EditProduct from './Product/EditProduct';
import ProductDetail from './Product/ProductDetail';
import Cart from './Product/Cart';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <App>
        <Routes>
            
        <Route index path='/' element={<Home2/> } />
         {/* <Route index path='/' element={<Home/> } /> */}
        {/* <Route path='/demo' element={<Demo/>} /> */}
        <Route path='/blog/list' element={<List/>} />
        <Route path='/blog/detail/:id' element={<Detail/>} />
        <Route path='/login' element={<Index/>} />
        <Route path='/account' element={<Update/>} />
        <Route path='/myproduct' element={<MyProduct/>} />
        <Route path='/addproduct' element={<AddProduct/>} />
        <Route path='/editproduct/:id' element={<EditProduct/>} />
        <Route path='/productdetail/:id' element={<ProductDetail/>} />
        <Route path='/cart' element={<Cart/>} />
       

        </Routes>
      </App>
    
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

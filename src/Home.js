import React, { Component, useContext, useEffect, useState }  from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { UserContext } from './UserContext';
function Home(props) {
    const [data,setData]=useState([])

    const getContext=useContext(UserContext)
    // console.log(getContext)

    useEffect(()=>{
        axios.get("http://api-local.com/api/product")
        .then(res =>{
            
            console.log(res)
            setData(res.data.data)
            // setId(res.data.data.id_user)
        })
        .catch(error=> console.log(error))

    },[])
    function renderData(){
      if(data.length>0){
        return data.map((value,key)=>{
          // console.log(value.id_user)
          let getImg=JSON.parse(value.image)
          // console.log(getImg)
          return(
            <div className="col-sm-4">
          <div className="product-image-wrapper">
            <div className="single-products">
              <div className="productinfo text-center">
                <img className="anh1" src={"http://api-local.com/upload/user/product/"+value.id_user+"/"+ getImg[0]}  alt="" />
                <h2 className="price">{value.price}</h2>
                <p>{value.detail}</p>
                <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
              </div>
              <div className="product-overlay">
                <div className="overlay-content" id="product1">
                  <h2>{value.price}</h2>
                  <p>{value.detail}</p>
                  <a id={value.id} onClick={renderCart} className="btn btn-default add-to-cart">
                    <i className="fa fa-shopping-cart" />Add to cart
                    
                  </a>
                </div>
              </div>
            </div>
            <div className="choose">
              <ul className="nav nav-pills nav-justified">
                <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                <li><Link to={"/productdetail/" + value.id}><i className="fa fa-plus-square" />More</Link></li>
              </ul>
            </div>
          </div>
            </div>
          )
        })
      }
    }
    function renderCart(e){
      let getId=e.target.id
      // console.log(getId)
      let productCart={}
      let getLocal=localStorage.getItem("cart")
      let check=1
      // {
      //   46: 1
      //   51: 1
      //   52: 1
      //   53: 1
      // }
      if(getLocal){
        productCart =JSON.parse(getLocal)
        Object.keys(productCart).map((key,index)=>{
          if(getId==key){
            check=2
            productCart[key]+=1
          }
        })

      }
      // console.log(productCart)
      if(check==1){
         productCart[getId]= 1
      }
       console.log(productCart)
      getContext.getData(productCart)
     
           
    
      
      localStorage.setItem("cart",JSON.stringify(productCart))



    }
    return (
      <div className="col-sm-9 padding-right">
          <div className="features_items">{/*features_items*/}
            <h2 className="title text-center">Features Items</h2>
              {renderData()} 
          </div>
      </div>
    );
  }
  
  export default Home;
  
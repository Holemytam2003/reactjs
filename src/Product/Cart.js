import React, { Component, useEffect, useState }  from 'react';
import axios from 'axios';
function Cart() {
    const [data,setData]=useState([])

    const [qty,setQty]=useState([])
    
    useEffect(()=>{
        let getLocal=localStorage.getItem("cart")
        if(getLocal){
            getLocal=JSON.parse(getLocal)
            axios.post("http://api-local.com/api/product/cart",getLocal)
            .then(res =>{
              //  console.log(res) 
               setData(res.data.data)
                // setQty(res.data.data)
                
            })
             .catch(error=> console.log(error))
             }
       

    },[])
    function renderTable(){
      if(data.length>0){
        return data.map((value,key)=>{
          let getImg=JSON.parse(value.image)
          //  console.log(getImg)
          return(
           
                <tr>
                  <td className="cart_product">

                     <img className='anh2' src={"http://api-local.com/upload/user/product/"+ value.id_user + "/" + getImg[0]}  alt="" />
                   
                  </td>
                   <td className="cart_description">
                    <h4><a href>Colorblock Scuba</a></h4>
                    <p>Web ID: {value.id}</p>
                  </td>
                  <td className="cart_price">
                    <p>${value.price}</p>
                  </td>
                  <td className="cart_quantity">
                    <div className="cart_quantity_button">
                      <a id={value.id} onClick={hanldeUp}  className="cart_quantity_up" href> + </a>
                                                                                          {/* nho set lai de tang qty */}
                      <input className="cart_quantity_input" type="text" name="quantity" value={value.qty} autoComplete="off" size={2} />
                      <a id={value.id} onClick={hanldeDown} className="cart_quantity_down" href> - </a>
                    </div>
                  </td>
                  <td className="cart_total">
                    <p className='cart_total_price'>
                        ${value.price * value.qty}
                    </p>
                        
                   
                  </td>
                  <td className="cart_delete">
                    <a id={value.id} onClick={handleDelete} className="cart_quantity_delete" href><i className="fa fa-times" /></a>
                  </td>
                </tr>
              
          )
        })
      }
    }
    function hanldeUp(e){
      let getId=e.target.id
      // copy ra 1 mang moi 
       let getLocal=localStorage.getItem("cart")
      const xx = [...data];
     
      xx.map((value,key)=>{
        if(getId==value.id){
          xx[key].qty +=1
        }  

      })
      setData(xx)
     
      if(getLocal){
        getLocal=JSON.parse(getLocal)
        // console.log(getLocal)
        Object.keys(getLocal).map((key,index)=>{
          if(getId==key){
            getLocal[key]+=1
          }
        })
      }
      localStorage.setItem("cart",JSON.stringify(getLocal))
    }   
   
    function hanldeDown(e){
     let getId=e.target.id
     let yy=[...data]
     let getLocal=localStorage.getItem("cart")
      // console.log(yy)
     yy.map((value,key)=>{
          if(getId==value.id){
            if(value.qty>1){
              yy[key].qty -=1  
            
            }
            else{
              delete yy[key]
             
            }
          }   
        setData(yy)  
      })
      if(getLocal){
        getLocal=JSON.parse(getLocal)
        Object.keys(getLocal).map((key,index)=>{
          if(getId==key){
            if(index>1){
            getLocal[key]-=1
             } else{
              delete getLocal[getId]
             }
          }
        })
      }

      localStorage.setItem("cart",JSON.stringify(getLocal))
     
    }

   function handleDelete(e){
          let getId=e.target.id
          let getLocal=localStorage.getItem("cart")
          let zz=[...data]
          zz.map((value,key)=>{
            if(getId==value.id){
              delete zz[key]
            }
          })
          setData(zz)
          if(getLocal){
            getLocal=JSON.parse(getLocal)
            delete getLocal[getId]
          }
          localStorage.setItem("cart",JSON.stringify(getLocal))
         
   }
  //  console.log(data)
   function renderTotal(){
    //  console.log(data)
      if(data.length>0){
        let sum = 0
        data.map((value,key)=>{
          let tong=value.qty * value.price
           sum= sum + tong
        })
        //  console.log(sum)
        return(
          <span>
            {sum}
          </span>
        )
      }
      
   }

   
    return (
        <section id="cart_items">
        <div className="container">
          <div className="breadcrumbs">
            <ol className="breadcrumb">
              <li><a href="#">Home</a></li>
              <li className="active">Shopping Cart</li>
            </ol>
          </div>
          <div className="table-responsive cart_info table_cart"  >
           
            <table className="table table-condensed table_second" >
              <thead>
                <tr className="cart_menu">
                  <td className="image">Item</td>
                  <td className="description" />
                  <td className="price">Price</td>
                  <td className="quantity">Quantity</td>
                  <td className="total">Total</td>
                  <td />
                </tr>
              </thead>
              <tbody>
              {renderTable()}
              </tbody>
            </table>
           
          </div>
          <div className='col-sm-6'>
              <div className="total_area">
                  <ul>
                    <li>Cart Sub Total <span>$59</span></li>
                    <li>Eco Tax <span>$2</span></li>
                    <li>Shipping Cost <span>Free</span></li>
                    <li>Total 
                      {renderTotal()}
                    </li>
                  </ul>
                <a className="btn btn-default update" href>Update</a>
                <a className="btn btn-default check_out" href>Check Out</a>
            </div>
          </div>
        </div>
        </section> 
        
    )
  }
  
  export default Cart;
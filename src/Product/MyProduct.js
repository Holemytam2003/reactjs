import React, { Component, useEffect, useState }  from 'react';
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';

function MyProduct() {
    const navigate= useNavigate()

    const [productData,setProduct]=useState({})



    function handleAdd(){
         navigate("/addproduct")
    }
    useEffect(()=>{
        let getToken=localStorage.getItem("token")
            getToken=JSON.parse(getToken)
            // console.log(getToken)
        let url="http://api-local.com/api/user/my-product"
        let config={
            headers:{
                    'Authorization':'Bearer ' + getToken,
                    'Content-Type':'application/x-www-form-urlencode',
                    'Accept':'application/json',
                }
          };
        axios.get(url,config)
        
        .then(res=>{
            //   console.log(res.data.data)
              setProduct(res.data.data)
        })
        .catch(error=>console.log(error))  
    },[])
    function handleProduct(){
    
      if(Object.keys(productData).length>0){
        //   console.log(productData.id)
           return Object.keys(productData).map((keys,index)=>{
                let getImg=JSON.parse(productData[keys].image)
                // console.log(getImg)
                let idUser= productData[keys].id_user

               return(
                <tbody>
                <tr>
                <td className='product_id' >
                    <p>{productData[keys].id}</p>
                
                </td>
                <td className='product_name'>
                    <p>{productData[keys].name}</p>
                </td>
                <td className='product_img'>
                    <a href>
                 
                       <img id="hanldeimg" src={"http://api-local.com/upload/user/product/"+ idUser +"/"+  getImg[0]}/>
                    </a>
                </td>
                <td className='product_price'>
                    <p>${productData[keys].price}</p>
                </td>
                <td>
                    <a>
                        <Link to={"/editproduct/" + productData[keys].id  }>
                         <i className="glyphicon glyphicon-pencil"/>
                         </Link>
                    </a>
                </td>
                <td className="cart_delete">
                    <a id={productData[keys].id} onClick={handleDelete}  className="cart_quantity_delete"  ><i className="fa fa-times" /></a>
                </td>
                </tr>
            </tbody>
               )
           })
          
      }
   
    }
    
    function handleDelete(e){
                      
        // let getId=e.target.id      
        // console.log(e.target.id)  
        
        let url="http://api-local.com/api/user/delete-product/" +e.target.id 
        let getToken=localStorage.getItem("token")
            getToken=JSON.parse(getToken)
        let config={
                headers:{
                        'Authorization':'Bearer ' + getToken,
                        'Content-Type':'application/x-www-form-urlencode',
                        'Accept':'application/json',
                    }
        };
        axios.get(url,config)
        .then(res=>{
            //  console.log(res)
            setProduct(res.data.data)
            // localStorage.setItem("product",JSON.stringify(productData))
             
        })
        .catch(error=>console.log(error))  
        
    }
    

   
    return (
       <div  className="col-md-9">
       
        <div className="table-responsive cart_info">
            <table className="table table-condensed"  >

                <thead>
                    <tr className="cart_menu">
                    <td>Id</td>
                    <td> Name</td>
                    <td>Image</td>
                    <td>Price</td>
                    <td>Action</td>
                    <td />
                    </tr>
                </thead>
                {handleProduct()}
               
            </table>
            
            <button id="myproduct" onClick={handleAdd}>Add New</button>
        </div>

       </div>
    )
  }
  
  export default MyProduct;
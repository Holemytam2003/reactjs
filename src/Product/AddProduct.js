import React, { Component, useEffect, useState }  from 'react';
import axios from 'axios';
import HandleErr from '../Member/HandleErr';
import { useNavigate } from 'react-router-dom';
function AddProduct() {
    const [input,setInput]=useState({
        name:"",
        price:"",
        category:"",
        brand:"",
        company:"",
        file:"",
        detail:"",
        sale:"",
        status:1,

    })

    const [errors,errInput]=useState({})

    const [brandData,setBrand]=useState([])

    const [categoryData,setCategory]=useState([])

    const[getFile,setFile]=useState("")

    const navigate=useNavigate()

    useEffect(()=>{
        axios.get("http://api-local.com/api/category-brand")
        .then(res =>{
             console.log(res)
             setBrand(res.data.brand)
             setCategory(res.data.category)
           
        })
        .catch(error=> console.log(error))
    },[])

    function optionBrand(){
        return brandData.map((item,key)=>{
            return(
              <option value={item.id}>{item.brand}</option>
            )
          })
    }

    function optionCategory(){
        return categoryData.map((item,key)=>{
            return(
              <option value={item.id}>{item.category}</option>
            )
          })
    }

    function hanldeInput(e){
        const keyInput=e.target.name
        const valueInput=e.target.value
        setInput(state=>({...state,[keyInput]:valueInput}))
    }

    function handleFile(e){
        const file = e.target.files;
        setFile(file)
        
    }

    function checkForm(e){
       
        e.preventDefault()
        let loi={}
        let flag=true
        let typeImg=["png", "jpg", "jpeg", "PNG", "JPG"]

        if(input.name==""){
            loi.name="PLEASE INPUT NAME"
            flag=false
        }

        if(input.price==""){
            loi.price="PLEASE INPUT PRICE"
            flag=false
        }

        if(input.category==""){
            loi.category="PLEASE INPUT CATEGORY"
            flag=false
        }

        if(input.brand==""){
            loi.brand="PLEASE INPUT BRAND"
            flag=false
        }

        if(input.company==""){
            loi.company="PLEASE INPUT COMPANY PROFILE"
            flag=false
        }
        // if(input.sale==""){
        //     loi.sale="PLEASE INPUT SALE"
        //     flag=false
        // }

        if(getFile){
            // => cách kiểm tra nhieu hinh anh
           if(Object.keys(getFile).length <= 3){
                Object.keys(getFile).map((key,index)=>{
                // console.log(getFile[key].size)
                    let getType= getFile[key].type
                    // console.log(getType)
                    let getName=getType.split("/")
                    // console.log(getName)

                    if(getFile[key].size > 1024*1024){
                        loi.file="FILE QUA LON"
                        flag=false
                    }else if(!typeImg.includes(getName[1])){
                        flag=false
                        loi.file="PLEASE UPLOAD IMAGE"
                    }
        
                })
            } else{
                flag=false
                loi.file="nhieu qua 3 img"
            }
        }else{
            loi.file="PLEASE INPUT FILE"
            flag=false
        }
        
        if(input.detail==""){
            loi.detail="PLEASE INPUT DETAIL"
            flag=false
        }

        if(flag==true){
            //  alert("ADD SUCCEED")
            
            let url="http://api-local.com/api/user/add-product"
            let getToken= localStorage.getItem("token")
                getToken=JSON.parse(getToken)
            let config={
                headers:{
                        'Authorization':'Bearer ' + getToken,
                        'Content-Type':'application/x-www-form-urlencode',
                        'Accept':'application/json',
                    }
              };
              const formData= new FormData();
                formData.append('name', input.name);
                formData.append('price', input.price);
                formData.append('category', input.category);
                formData.append('brand', input.brand);
                formData.append('company', input.company);
                formData.append('detail', input.detail);
                formData.append('status', input.status);

                Object.keys(getFile).map((item,i)=>{
                    formData.append('file[]', getFile[item]);
                })
                axios.post(url,formData,config)
                .then(res=>{
                    // console.log(res)
                    if(res.data.errors){
                        errInput(res.data.errors)
                    }else{
                        errInput({})
                        // let getProduct=res.data.data
                        // localStorage.setItem("products",JSON.stringify(getProduct))

                        navigate("/myproduct")
                    }
                })
               .catch( errors=>console.log(errors))

                
              
        }else{
            errInput(loi)
        }
    }
    function renderSale(){
        if(input.status==1){
            return(
               <></>
            )
        }else{
            return(
                <div>
                <input  id='sale' type="text" onChange={hanldeInput}/>%
                </div>
            )
        }
        //  <input value={input.status} id='sale' type="text" onChange={hanldeInput}/>%
    }
    return (
      
        <div className="col-lg-8">
        <div className="signup-form">{/*sign up form*/}
        <h2>Create Product!</h2>
        <HandleErr errors={errors}/>
        <form action="#"  encType="multipart/form-data"  onSubmit={checkForm}>
            
            <input type="text" name='name' placeholder="Name" onChange={hanldeInput} />
            
            <input type="text" name='price' placeholder="Price" onChange={hanldeInput} />

            <select name='category' onChange={hanldeInput}>
                <option>Please choose category</option>
                {optionCategory()}
            </select>

            <select name='brand' onChange={hanldeInput}>
                <option>Please choose brand</option>
                {optionBrand()}
            </select>

            <select value={input.status} name='status' onChange={hanldeInput}>
                <option value="1">new</option>
                <option value="0">Sale</option>
            </select>

            {renderSale()}
         
            <input type="text" name='company' placeholder="Company Profile" onChange={hanldeInput} />
                    {/* multiple dung de chon nhieu img */}
            <input multiple type="file" name='avatar' onChange={handleFile}/>

            <textarea  name="detail" rows={11} defaultValue={""} placeholder='Detail' onChange={hanldeInput}/>
            
            <button type="submit" className="btn btn-default">Signup</button>
        </form>
        </div>{/*/sign up form*/}
        </div>
    
    )
  }
  
  export default AddProduct;
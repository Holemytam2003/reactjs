import React, { Component, useEffect, useState }  from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import HandleErr from '../Member/HandleErr';
function EditProduct() {
    let params=useParams()
    const[data,setData]=useState({
        name:"",
        price:"",
        category:"",
        brand:"",
        company:"",
        file:"",
        detail:""
    })
    const [errors,setErrors]=useState({})

    const [getImg,setImg]=useState([])
    // console.log(getImg)
   

    const [getId,setId]=useState("")
    // console.log(getId)
    const [getCategory,setCategory]=useState([])

    const [getBrand,setBrand]=useState([])

    const [odlImg,setoldImg]=useState([])
    // console.log(odlImg)

    const [getFile,setFile]=useState("")
    
    useEffect(()=>{
        let getToken=localStorage.getItem("token")
            getToken=JSON.parse(getToken)
            // console.log(getToken)
        let url="http://api-local.com/api/user/product/" + params.id
        let config={
            headers:{
                    'Authorization':'Bearer ' + getToken,
                    'Content-Type':'application/x-www-form-urlencode',
                    'Accept':'application/json',
                }
          };
        axios.get(url,config)
        
        .then(res=>{
            //   console.log(res)
              setData(res.data.data)
            //   console.log(res.data.data.image)
              setImg(res.data.data.image)
              setId(res.data.data.id_user)


             
        })
        .catch(error=>console.log(error))  
    },[])
    useEffect(()=>{
        axios.get("http://api-local.com/api/category-brand")
        .then(res =>{
            //  console.log(res)
                setBrand(res.data.brand)
                setCategory(res.data.category)
           
        })
        .catch(error=> console.log(error))
    },[])
    function optionCategory(){
       
           return getCategory.map((item,key)=>{
              return(
                        // ****
                <option value={item.id}>{item.category}</option>
              )
           })
    
    }
    function optionBrand(){
        return getBrand.map((item,key)=>{
            return(
                <option value={item.id}>{item.brand}</option>
            )
        })
    } 
    function renderForm(){
        if(Object.keys(data).length>0){
            
                return(
                    <>
                        <input value={data.name} type="text" name='name' placeholder="Name" onChange={hanldeInput} /> 
                        <input value={data.price} type="text" name='price' placeholder="Price" onChange={hanldeInput} />
                        <select value={data.id_category} name='id_category' onChange={hanldeInput}>
                            <option value="">Please choose category</option>
                            {optionCategory()}
                         </select>
                         <select value={data.id_brand} name='id_brand' onChange={hanldeInput}>
                            <option>Please choose brand</option>
                            {optionBrand()}
                        </select>
                        <select name='status' onChange={hanldeInput}>
                            <option>{data.status}</option>
                        </select>
                        <input value={data.company_profile} type="text" name='company_profile' placeholder="Company Profile" onChange={hanldeInput} />
                        <input multiple type="file" name='avatar' onChange={handleFile}/>
                        <ul className='image'>{renderImg()}</ul> 
                        <textarea value={data.detail} name="detail" rows={11} defaultValue={""} placeholder='Detail' onChange={hanldeInput}/>

                    </>
                )
            
        }
    }
    function handleFile(e){
        const file = e.target.files;
        setFile(file)
    }
    function renderImg(){
        if(getImg.length>0){
            return getImg.map((value,key)=>{
                return(
                
                    <li>    
                                                                                        {/* hinh anh */}
                        <img src={"http://api-local.com/upload/user/product/"+ getId  +"/" + value}/>
                        <input  type="checkbox" onClick={hanldeAvatar} value={value}/>
                    </li>

                
                )
            })
        }
    }
    function hanldeAvatar(e){
        let getValue = e.target.value;
        // console.log(getValue)

        //  checkbox co trang thai la check nen console.log len de coi
        let getCheck = e.target.checked;
        // console.log(getCheck)
   
        if(getCheck){
             //  => cach lay hinh anh dua vao mang
             setoldImg(state=>([...state, getValue]))
             
        } else{

            let result = odlImg.filter(function(elem){
                return elem != getValue; 
             });
             setoldImg(result)
            

        }

    }
    //  console.log(odlImg)
    function hanldeInput(e){
        const keyInput=e.target.name
        const valueInput=e.target.value
        setData(state=>({...state,[keyInput]:valueInput}))
    }
    function checkForm(e){
        e.preventDefault()
        let loi={}
        let flag=true
        let typeImg=["png", "jpg", "jpeg", "PNG", "JPG"]

        if(data.name==""){
            loi.name="PLEASE INPUT NAME"
            flag=false
        }
        if(data.price==""){
            loi.price="PLEASE INPUT PRICE"
            flag=false
        }

        if(data.category==""){
            loi.category="PLEASE INPUT CATEGORY"
            flag=false
        }
        if(data.brand==""){
            loi.brand="PLEASE INPUT BRAND"
            flag=false
        }

        if(data.company==""){
            loi.company="PLEASE INPUT COMPANY PROFILE"
            flag=false
        }
        if(data.detail==""){
            loi.detail="PLEASE INPUT DETAIL"
            flag=false
        }
        // console.log(getFile)
        if(getFile){
            if(Object.keys(getFile).length <=3){
                Object.keys(getFile).map((keys,index)=>{
                    let getType=getFile[keys].type
                    // console.log(getType)
                    let getName=getType.split("/")
                    // console.log(getName)
                    if(getFile[keys].size > 1024*1024){
                        loi.file="THE FILE'S SIZE IS TOO BIG"
                        flag=false
                    }else if(!typeImg.includes(getName[1])){
                        loi.file ="PLEASE UPLOAD IMAGE"
                        flag=false
                    }
                })
            } 
        }
        if(getImg.length + getFile.length >3){
            flag=false
            loi.file="CAN'T UPLOAD MORE 3 IMAGES"
        }

        if(flag==true){
            // alert("ok")
            // user/edit-product/{id}
            let url="http://api-local.com/api/user/edit-product/" + params.id
            let getToken= localStorage.getItem("token")
                getToken=JSON.parse(getToken)
            let config={
                headers:{
                        'Authorization':'Bearer ' + getToken,
                        'Content-Type':'application/x-www-form-urlencode',
                        'Accept':'application/json',
                    }
              };
              const formData= new FormData();
                formData.append('name', data.name);
                formData.append('price', data.price);
                formData.append('category', data.id_category);
                formData.append('brand', data.id_brand);
                formData.append('company', data.company);
                formData.append('detail', data.detail);
                formData.append('status', data.status);

                Object.keys(getFile).map((item,i)=>{
                    formData.append('file[]', getFile[item]);
                })
                Object.keys(odlImg).map((item,i)=>{
                    formData.append('avatarCheckBox[]', odlImg[item]);
                })
                axios.post(url,formData,config)
                .then(res=>{
                     console.log(res)
                     if(res.data.errors){
                        setErrors(res.data.errors)
                    }else{
                        setErrors({})
                    }
                })
               .catch( errors=>console.log(errors))

        } else{
            setErrors(loi)
        }

    }

    return (
       
            <div className="col-lg-8">
                <div className="signup-form">{/*sign up form*/}
                <HandleErr errors={errors}/>
                <form action="#"  encType="multipart/form-data"  onSubmit={checkForm}>
                    {renderForm()}                    
                    <button type="submit" className="btn btn-default">Signup</button>
                </form>
                </div>{/*/sign up form*/}
            </div>
        
    )
  }
  
  export default EditProduct;
  
import axios from 'axios';
import React, { Component, useState }  from 'react';
import HandleErr from './HandleErr';
function Register() {
    const [input,setInput]=useState({
        name:"",
        email:"",
        password:"",
        phone:"",
        address:"",
        level:0,
    })
    const[errors,errInput]=useState({})

    const[getFile,setFile]=useState("")

    const[avatar,setAvatar]=useState("")

    function handleFile(e){
        const file = e.target.files;
            //mã hoá gửi qua api
        // send file to api server
        let reader = new FileReader();
        reader.onload = (e) => {
            setAvatar(e.target.result) //cai nay de gui qua api
            setFile(file) //cai nay de toan bo thong file upload vao file de xuong check validate
        
        };
        reader.readAsDataURL(file[0]);
    }
    
    function handleInput(e){
        const keyInput=e.target.name
        const valueInput=e.target.value
        setInput(state=>({...state,[keyInput]:valueInput}))
    }

    function checkForm(e){
        e.preventDefault()
        let loi={}
        let x=1
        let checkEmail=/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        let typeImg=["png", "jpg", "jpeg", "PNG", "JPG"]

        
        if(input.name==""){
            x=2
            loi.name="PLEASE INPUT NAME"
        }
        if(input.email==""){
            x=2
            loi.email="PLEASE INPUT EMAIL"
        } else{
            if(!checkEmail.test(input.email)){
                x=2
                loi.email="ERROR EMAIL"
            }
        }

        if(input.password==""){
            x=2
            loi.password="PLEASE INPUT PASSWORD"
        }
        if(input.phone==""){
            x=2
            loi.phone="PLEASE INPUT PHONE"
        }
        if(input.address==""){
            x=2
            loi.address="PLEASE INPUT ADDRESS"
        }
        // console.log(getFile)
        if(getFile){
            let getType=getFile[0].type
            // console.log(getType)
            let getImg=getType.split("/")
            // console.log(getImg)
            if(getFile[0].size>1024*1024){
                x=2
                loi.avatar="file qua lon"
            } else if(!typeImg.includes(getImg[1])){
                x=2
                loi.avatar="vui long upload anh"
            }
        } else{
            x=2
            loi.avatar="vui long nhap anh"
        }
        
        if(x==1){
            const data = {
                name:input.name,
                email:input.email,
                password:input.password,
                phone:input.phone,
                address: input.address,
                avatar:avatar,
                level: 0
            }
            axios.post("http://api-local.com/api/register", data)
            .then(res => {
                console.log(res)
                if(res.data.errors){
                    errInput(res.data.errors)
                }else{
                    alert("ok")
                    errInput({})
                }

            }
            )
            .catch(error => console.log(error))
        }else{
            // console.log(loi)
            errInput(loi)
        }
        



    }
    return (
        <div className="col-sm-4">
            <div className="signup-form">{/*sign up form*/}
            <h2>New User Signup!</h2>
            <HandleErr errors={errors}/>
            <form action="#" onSubmit={checkForm} encType="multipart/form-data" >
                
                <input type="text" name='name' placeholder="Name" onChange={handleInput} />
                
                <input type="email" name='email' placeholder="Email Address" onChange={handleInput} />
             
                <input type="password" name='password' placeholder="Password" onChange={handleInput} />

                <input type="number" name='phone' placeholder='Phone' onChange={handleInput}/>

                <input type="text" name='address' placeholder='Address' onChange={handleInput}/>

                <input type="file" name='avatar' onChange={handleFile}/>
                
                <button type="submit" className="btn btn-default">Signup</button>
            </form>
            </div>{/*/sign up form*/}
      </div>
    )
  }
  
  export default Register;
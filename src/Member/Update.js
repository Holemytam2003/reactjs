import React, { Component, useEffect, useState }  from 'react';
import axios from 'axios';
import HandleErr from './HandleErr';
function Update() {
    const [update,setUpdate]=useState({
        name:"",
        email:"",
        password:"",
        phone:"",
        address:"",

    })

    const[avatar,setAvatar]=useState("")
    // console.log(update.id)
    const[getFile,setFile]=useState("")

    const [errors,setErrors]=useState({})


    useEffect(()=>{
      let getLocal=localStorage.getItem("information")
      if(getLocal){
        getLocal=JSON.parse(getLocal)
        //  console.log(getLocal)
        setUpdate(getLocal)
      }
    },[])
    function handleInput(e){

      const keyInput=e.target.name
      const valueInput=e.target.value
      setUpdate(state=>({...state,[keyInput]:valueInput}))
    
      }

    function handleFile(e){
      const file = e.target.files;
   
       // send file to api server
       let reader = new FileReader();
       reader.onload = (e) => {
        setAvatar(e.target.result) //cai nay de gui qua api
        setFile(file) //cai nay de toan bo thong file upload vao file de xuong check validate
    
    };
    reader.readAsDataURL(file[0]);
    }

    function checkForm(e){
      e.preventDefault()
      let loi={}
      let flag=true
      if(update.name==""){
        loi.name="PLEASE INPUT NAME "
        flag=false
      }

      if(update.phone==""){
        loi.phone="PLEASE INPUT PHONE"
        flag=false
      }

      if(update.address==""){
        loi.address="PLEASE INPUT ADDRESS"
        flag=false
      }



      if(flag==true){
        // alert("ok")
        let url="http://api-local.com/api/user/update/" + update.id
        let getToken=localStorage.getItem("token")
            getToken=JSON.parse(getToken)
            // console.log(getToken)
        let config={
            headers:{
                    'Authorization':'Bearer ' + getToken,
                    'Content-Type':'application/x-www-form-urlencode',
                    'Accept':'application/json',
                }
          };
        const formData= new FormData();
              formData.append('name', update.name);
              formData.append('email', update.email);
              formData.append('password', update.password ? update.password : "");
              formData.append('phone', update.phone);
              formData.append('address', update.address);
              formData.append('avatar', update.avatar);

        
         axios.post(url,formData,config)
              .then(res=>{
                   console.log(res)
                   localStorage.setItem("information",JSON.stringify(update))
                   let getToken=res.data.success.token
                  //  console.log(getToken)
                  localStorage.setItem("token",JSON.stringify(getToken))
                  
              })
              .catch( errors=>console.log(errors))
          
    
              
        
      }else{
        setErrors(loi)
      }


    }

    useEffect(()=>{
      let getUpdate=localStorage.getItem("information")
      if(getUpdate){
        // console.log(getUpdate)
        getUpdate=JSON.parse(getUpdate)
      }
   
    },[])

    function renderUpdate(){
      if(Object.keys(update).length>0){
        return(
          <>
            <input type="text" name='name' placeholder="Name" value={update.name} onChange={handleInput} />
            <input type="email" name='email' placeholder="Email Address" value={update.email}  readonly/>
            <input type="password" name='password' placeholder="Password"  onChange={handleInput} />
            <input type="number" name='phone' placeholder='Phone' value={update.phone} onChange={handleInput}/>
            <input type="text" name='address' placeholder='Address' value={update.address} onChange={handleInput}/>
            <input type="file" name='avatar' onChange={handleFile}/>
            <img id='update' src= {"http://api-local.com/upload/user/avatar/" + update.avatar}/>
           
          </>
        )
      }
    }
    return (
      <div className='signup-form'>
        <div className="col-lg-8">
              <div className="signup-form">{/*sign up form*/}
              <h2> User Update!</h2>
              <HandleErr errors={errors}/>
              <form action="#" onSubmit={checkForm} encType="multipart/form-data" >
                  {renderUpdate()}                                   
                  {/* <input type="file" name='avatar' onChange={handleFile} /> */}
                  
                  <button type="submit" className="btn btn-default">Signup</button>
              </form>
              </div>{/*/sign up form*/}
        </div>
      </div>
    )
  }
  
  export default Update;
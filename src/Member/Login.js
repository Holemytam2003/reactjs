import axios from 'axios';
import React, { Component, useState }  from 'react';
import { useNavigate } from 'react-router-dom';
import HandleErr from './HandleErr';
function Login() {
    const [input,setInput]=useState({
      email:"",
      password:"",
      level:"",
    })
    const [errors,errInput]=useState({})
    const navigate=useNavigate();
    
    function hanldeInput(e){
      const keyInput=e.target.name
      const valueInput=e.target.value
      setInput(state=>({...state,[keyInput]:valueInput}))
    }
    function checkForm(e){
      e.preventDefault()
      // console.log(123)
      let loi={}
      let flag=true
      let checkEmail=/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if(input.email==""){
        flag=false
        loi.email="PLEASE INPUT NAME"
      }else{
        if(!checkEmail.test(input.email)){
          flag=false
          loi.email="ERROR EMAIL"
        }
      }

      if(input.password==""){
        flag=false
        loi.password="PLEASE INPUT PASSWORD"
      }
  
      if(flag==true){
        //  console.log("thanh cong")
        errInput({})
        const data={
          email: input.email,
          password:input.password,
          level:0
        }
        axios.post("http://api-local.com/api/login",data)
        .then(res => {
          //  console.log(res)
          if(res.data.errors){
            errInput(res.data.errors)
          }else{
            // tao 1 bien bang true -> dua vao local (json string)
            
            errInput({})
            

            let getData=res.data.Auth
            // console.log(getData)
            localStorage.setItem("information",JSON.stringify(getData))

            let getToken=res.data.success.token
            localStorage.setItem("token",JSON.stringify(getToken))
          
            localStorage.setItem("isLogin",JSON.stringify(true))
            navigate("/")

            
            
          
          }
        })
        .catch( errors=>console.log(errors))
        
      }else{
        errInput(loi)
      }
    }
    return (
      <div className="col-sm-4 col-sm-offset-1">
        <div className="login-form">{/*login form*/}
          <h2>Login to your account</h2>
          <HandleErr errors={errors}/>
          <form action="#" onSubmit ={checkForm}>

            <input type="email" placeholder="Email Address" name="email" onChange={hanldeInput}/>

            <input type="password" name='password' placeholder="Password" onChange={hanldeInput} />

            
            <span>
              <input type="checkbox" className="checkbox" /> 
              Keep me signed in
            </span>
            <button type="submit" className="btn btn-default">Login</button>
          </form>
        </div>{/*/login form*/}
      </div>
    )
  }
  
  export default Login;
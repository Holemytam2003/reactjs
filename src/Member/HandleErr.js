import React, { Component }  from 'react';
function HandleErr(props) {
    function renderData(){
        let errors=props.errors
        // console.log(errors)
        if(Object.keys(errors).length>0){
            return Object.keys(errors).map((key,index)=>{
                return(
                    <li key={index}>{errors[key]}</li>
                )
            })
        }
    }
    return (
      <div className="App">
        {renderData()}
      </div>
    )
  }
  
  export default HandleErr;
import React, { Component, useState }  from 'react'
import './App.css';
import Header from './Layout/Header';
import Footer from './Layout/Footer';
import MenuLeft from './Layout/MenuLeft';
import { UserContext } from './UserContext';
// import { Provider } from 'react-redux';
// import store from './Store';

function App(props) {
  const [getCart,setCart]=useState({})
  function getData(data){
    setCart(data)
  }
  return (
    <UserContext.Provider value={{
          getData:getData
    }}>
         <Header getCart={getCart}/>
            <section>
              <div className='container'>
                <div className='row'>
                  
                  <MenuLeft/>
                  {props.children}
                </div>
              </div>
            </section>
        <Footer/>
    </UserContext.Provider>
  );
}


export default App;

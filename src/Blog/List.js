import React, { Component, useEffect, useState }  from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
function List() {
    const[data,setData]=useState([])
    useEffect(()=>{
      axios.get("http://api-local.com/api/blog")
      .then(res=>{
        // console.log(res.data.blog.data)
        setData(res.data.blog.data)
      })
      .catch(error=>console.log(error))  
    },[])
    function renderData(){
      if(data.length>0){
        return data.map((value,key)=>{
          return(
            <div className="single-blog-post">
              <h3>{value.title}</h3>
               <div className="post-meta">
                <ul>
                  <li><i className="fa fa-user" /> Mac Doe</li>
                  <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                  <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                </ul>
                <span>
                  <i className="fa fa-star" />
                  <i className="fa fa-star" />
                  <i className="fa fa-star" />
                  <i className="fa fa-star" />
                  <i className="fa fa-star-half-o" />
                </span>
              </div>
              <a href>
                            {/* cach lay hinh anh tu api */}
                <img src={"http://api-local.com/upload/Blog/image/" + value.image} alt="" />
              </a>
              <p>{value.description}</p>
                                                    {/* cach lay id tu Api */}
              <Link className="btn btn-primary" to={"/blog/detail/" + value.id}>Read More</Link>
            </div>
          )
        })
      }
    }
    return (
      <div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
            {renderData()}
          </div>
      </div>
    )
  }
  
  export default List;
  
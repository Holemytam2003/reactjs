import React, { Component, useEffect, useState }  from 'react';
import { useParams } from 'react-router-dom';
import StarRatings from 'react-star-ratings';
import axios from 'axios';
function Rate() {
    const [rating, setRating] = useState(2)
    
    let getIdblog=useParams()
    //  console.log(getIdblog.id)

    
    function changeRating( newRating, name ) {
      setRating(newRating)
      let checkLogin=localStorage.getItem("isLogin")
      if(checkLogin){
          checkLogin=JSON.parse(checkLogin)
          if(!checkLogin){
              alert("PLEASE LOGIN")
          }else{
            let getToken=localStorage.getItem("token")
                getToken=JSON.parse(getToken)
            let getData=localStorage.getItem("information")
                getData=JSON.parse(getData)

            let url="http://api-local.com/api/blog/rate/" + getIdblog.id;

            let config={
                headers:{
                    'Authorization':'Bearer ' + getToken,
                    'Content-Type':'application/x-www-form-urlencode',
                    'Accept':'application/json',
                }
            };
            if(newRating){
                const formData= new FormData();
                    formData.append('blog_id', getIdblog.id);
                    formData.append('user_id', getData.id);
                    formData.append('rate', newRating);
                    axios.post(url,formData,config)
                    .then(res=>{
                         console.log(res)
                        // tong diem danh gia / tong so user
                    })
                    .catch( errors=>console.log(errors))
            }else{
                 alert("PLEASE RATE")
            }
        }

      }else{
        alert("PLEASE LOGIN")
      }
    }
    //  Khi reload trang detail thi tinh trung binh công va hien thi ra trên số ngôi sao tương ứng        						
    useEffect(()=>{
        axios.get("http://api-local.com/api/blog/rate/" + getIdblog.id )
        .then(res=>{
            // tong diem danh gia / tong so user
            console.log(res)
            let getData=res.data.data
            if(Object.keys(getData).length>0){

                //  muon dem trong obj co bnhieu ptu:
                    // getLength la tong so user
                let getLength=Object.keys(getData).length
                let sum=0
                Object.keys(getData).map(function(key,index){
                    let getRate=getData[key].rate
                    //  console.log(getRate)
                    // sum la tong diem danh gia
                    sum = sum + getRate
                
                })
                // console.log(sum)
                setRating(sum/getLength)
                



            }
            // const arr=[1,2,3,4]
            // let sum=0
            // arr.map(function(value,key){
            //     sum=sum+value
                
            // })
            // console.log(sum)


            // tu duy 
            // nho kien  thuc
            


        })
        .catch(errors=>console.log(errors))
    },[])


    return (
        <div className="rating-area">
            <ul className="ratings">
            <li className="rate-this">Rate this item:</li>
            <li>
            <StarRatings
                rating={rating}
                starRatedColor="blue"
                changeRating={changeRating}
                numberOfStars={6}
                name='rating'
            />
            </li>
            <li className="color">(6 votes)</li>
            </ul>
            <ul className="tag">
            <li>TAG:</li>
            <li><a className="color" href>Pink <span>/</span></a></li>
            <li><a className="color" href>T-Shirt <span>/</span></a></li>
            <li><a className="color" href>Girls</a></li>
            </ul>
        </div>
    )
  }
  
  export default Rate;
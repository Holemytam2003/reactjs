import React, { Component, useEffect, useState }  from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import ListComment from './ListComment';
import Comment from './Comment';
import Rate from './Rate';
function Detail() {
    //  params :tham so tren url
    let params=useParams()
    // console.log(params)
   
    const [data,setData]=useState([])
    const [listComment,setListComment]=useState([])
    const [getId,setId]=useState("")
    useEffect(()=>{
        axios.get("http://api-local.com/api/blog/detail/"+params.id)
        .then(res=>{
            //  console.log(res.data.data)
            // console.log(res.data.data.comment)
            setData(res.data.data)
            // set list comment nằm trong list comment
            setListComment(res.data.data.comment)

  
        })
        .catch(error=>console.log(error))  
      },[])

      function getComment(data){
        // console.log(data)
        let concatComment= listComment.concat(data)
        setListComment(concatComment)
        

      }
      function getIdCmt(id){
          //  console.log(id)
          setId(id)
        

      }
      
    
    function renderData(){
                    // cua obj
        if(Object.keys(data).length>0){
            
                return(
                  <div className="single-blog-post">
                    <h3>{data.title}</h3>
                    <div className="post-meta">
                      <ul>
                        <li><i className="fa fa-user" /> Mac Doe</li>
                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                      </ul>
                      {/* <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </span> */}
                    </div>
                    <a href>
                      <img src={"http://api-local.com/upload/Blog/image/"+ data.image} alt="" />
                    </a>
                    <p>{data.content}</p> <br />
                    <p>{data.content}</p> <br />
                    <p>
                      Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p> <br />
                    <p>
                      Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                    </p>
                    <div className="pager-area">
                      <ul className="pager pull-right">
                        <li><a href="#">Pre</a></li>
                        <li><a href="#">Next</a></li>
                      </ul>
                    </div>
                    
                  </div>
                  
                )

        }
    }
    return (
        <div class="col-sm-9">
            <div class="blog-post-area">
                <h2 class="title text-center">Latest From our Blog</h2>
                {renderData()}
            </div>

            <Rate/>

            {/*  */}
            <ListComment listComment={listComment} getIdCmt={getIdCmt}/>

            <Comment getComment={getComment} getId={getId}/>

            {/*  */}
        </div>
        

    )
  }
  
  export default Detail;
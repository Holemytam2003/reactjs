
import React, { Component, useState }  from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
function Comment(props) {
    let getIdblog=useParams()
    
    // console.log(getComment)
    // console.log(getIdblog)
    const[comment,setComment]=useState("")

    const [errorsComment,setErrcomment]=useState("")

    // console.log(props.getId)

    function hanldeComment(e){
        setComment(e.target.value)
    }
    function handleSumbit(e){
        e.preventDefault()
        // console.log(comment)
        let getLocal= localStorage.getItem("isLogin")
       
        // console.log(getLocal)
        if(getLocal){
            getLocal=JSON.parse(getLocal)    
            if(!getLocal){
                alert("PLEASE LOGIN")
            } else{
                let getToken=localStorage.getItem("token")
                    getToken=JSON.parse(getToken)
                

                //  duong dan api
                // console.log(getIdblog)
                let url="http://api-local.com/api/blog/comment/"+ getIdblog.id;
                
                //  config de gui token qua api
                let config={
                    headers:{
                        'Authorization':'Bearer ' + getToken,
                        'Content-Type':'application/x-www-form-urlencode',
                        'Accept':'application/json',
                    }
                };
                if(comment){
                    let userData=localStorage.getItem("information")
                         userData=JSON.parse(userData)
                    // console.log(userData.id)
                    
                    const formData= new FormData();
                    formData.append('id_blog', getIdblog.id);
                    formData.append('id_user', userData.id);
                    formData.append('id_comment', props.getId ? props.getId : 0);
                    formData.append('comment', comment);
                    formData.append('image_user', userData.avatar);
                    formData.append('name_user', userData.name);
                    
                    axios.post(url,formData,config)
                    .then(res=>{
                        // console.log(res)
                        props.getComment(res.data.data)


                    })
                    .catch( errors=>console.log(errors))



                }else{
                    setErrcomment("Please input comment")
                }


            }
        
        }
    }

    return (
        <div className="replay-box">
            <div className="row">
            <div className="col-sm-12">
                <h2>Leave a replay</h2>
                <div className="text-area">
                <div className="blank-arrow">
                    <label>Your Name</label>
                </div>
                <span>*</span>
                
                <textarea id="traloi" name="message" rows={11} defaultValue={""} onChange={hanldeComment}/>
                <p>{errorsComment}</p>
                <a className="btn btn-primary" href onClick={handleSumbit}>post comment</a>
                </div>
            </div>
            </div>
        </div>

    )
  }
  
  export default Comment;
  